package com.example.dawid.notatniktreningowy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView utworzNoweKonto;
    private Button zaloguj;
    private Button exit;
    Context context;

    private Button loginBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        utworzNoweKonto = (TextView) findViewById(R.id.utworz_konto_link);
        loginBut = (Button) findViewById(R.id.login_but);


        loginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(MainActivity.this, ListOfWorkout.class);
                startActivity(intent);
            }
        });

        utworzNoweKonto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginForm();
            }
        });
        exit = (Button) findViewById(R.id.exit_but);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void openLoginForm() {
        Intent intent = new Intent(getApplicationContext(), LoginForm.class);
        startActivity(intent);
    }
}
