package com.example.dawid.notatniktreningowy;

/**
 * Created by Dawid on 2017-08-30.
 */

public class IntentConstants {

    public final static int INTENT_REQUEST_CODE = 1;
    public final static int INTENT_RESULT_CODE = 1;

    public final static int INTENT_RESULT_CODE_TITLE = 1;

    public final static int INTENT_REQUEST_CODE_TWO = 2;
    public final static int INTENT_RESULT_CODE_TW0 = 2;

    public final static int INTENT_REQUEST_TITLE_FILED = 3;
    public final static int INTENT_RESULT_TITLE_DATA = 3;

    public final static String INTENT_DATE_FILED = "date_filed";
    public final static String INTENT_DATE_TITLE_DATA = "date_title_data";

    public final static String INTENT_TITLE_FILED = "title_filed";
    public final static String INTENT_TITLE_DATA = "title_data";

    public final static String INTENT_ITEM_POSITION = "item_position";
    public final static String INTENT_CHANGED_DATE = "changed_date";
    public final static String INTENT_CHANGED_TITLE = "changed_title";
}
