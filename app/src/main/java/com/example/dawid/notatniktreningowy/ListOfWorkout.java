package com.example.dawid.notatniktreningowy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class ListOfWorkout extends AppCompatActivity {

    ListView listOfWorkout;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    String dateOfTraining;
    String titleOfTraining;
    String dateAndTitle;

    int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_workout);

        listOfWorkout = (ListView) findViewById(R.id.list_of_workout);
        arrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
        listOfWorkout.setAdapter(arrayAdapter);
        listOfWorkout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(ListOfWorkout.this, EditDataTraining.class);
                intent.putExtra(IntentConstants.INTENT_DATE_TITLE_DATA, arrayList.get(position).toString());
                intent.putExtra(IntentConstants.INTENT_ITEM_POSITION, position);
                startActivityForResult(intent, IntentConstants.INTENT_REQUEST_CODE_TWO);
            }
        });

        try {
            Scanner scanner = new Scanner(openFileInput("DataTraining.txt"));
            while (scanner.hasNext()) {
                String data = scanner.nextLine();

                arrayAdapter.add(data);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        try {
            PrintWriter printWriter = new PrintWriter(openFileOutput("DataTraining.txt", Context.MODE_PRIVATE));
            for (String data : arrayList) {
                printWriter.println(data);
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        finish();

    }

    public void onClick(View v) {
        Intent intent = new Intent();
        intent.setClass(ListOfWorkout.this, TrainingData.class);
        startActivityForResult(intent, IntentConstants.INTENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == IntentConstants.INTENT_REQUEST_CODE) {
            dateOfTraining = data.getStringExtra(IntentConstants.INTENT_DATE_FILED);
            titleOfTraining = data.getStringExtra(IntentConstants.INTENT_TITLE_FILED);
            dateAndTitle = dateOfTraining + "\n" + titleOfTraining;
            arrayList.add(dateAndTitle);
            arrayAdapter.notifyDataSetChanged();
        } else if (resultCode == IntentConstants.INTENT_REQUEST_CODE_TWO) {
            dateOfTraining = data.getStringExtra(IntentConstants.INTENT_CHANGED_DATE);
            titleOfTraining = data.getStringExtra(IntentConstants.INTENT_CHANGED_TITLE);
            position = data.getIntExtra(IntentConstants.INTENT_ITEM_POSITION, -1);
            dateAndTitle = dateOfTraining + "\n" + titleOfTraining;
            arrayList.remove(position);
            arrayList.add(position, dateAndTitle);
            arrayAdapter.notifyDataSetChanged();

        }
    }
}
