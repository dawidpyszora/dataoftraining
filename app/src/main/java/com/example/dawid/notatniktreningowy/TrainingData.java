package com.example.dawid.notatniktreningowy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class TrainingData extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_data);
    }

    public void saveTrainingDataButton(View v) {
        String titleText = ((EditText) findViewById(R.id.title_edt)).getText().toString();
        String dateOfTraining = ((EditText) findViewById(R.id.date_of_training)).getText().toString();
        if (titleText.equals("")) {
            Toast.makeText(this, "You have to fill title", Toast.LENGTH_LONG).show();
        } else if (dateOfTraining.equals("")) {
            Toast.makeText(this, "You have to fill data", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent();
            intent.putExtra(IntentConstants.INTENT_DATE_FILED, dateOfTraining);
            intent.putExtra(IntentConstants.INTENT_TITLE_FILED, titleText);
            setResult(IntentConstants.INTENT_RESULT_CODE, intent);
            finish();
        }
    }
}
