package com.example.dawid.notatniktreningowy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class EditDataTraining extends AppCompatActivity {

    String dateText;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_data);

        Intent intent = getIntent();
        dateText = intent.getStringExtra(IntentConstants.INTENT_DATE_TITLE_DATA);
        position = intent.getIntExtra(IntentConstants.INTENT_ITEM_POSITION, -1);

        String text = dateText;
        String[] textArray = text.split("\n");
        String date = textArray[0];
        String title = textArray[1];

        EditText dateData = (EditText) findViewById(R.id.date_of_training);
        EditText titleData = (EditText) findViewById(R.id.title_edt);

        dateData.setText(date);
        titleData.setText(title);
    }

    public void saveTrainingDataButton(View v) {
        String changedDate = ((EditText) findViewById(R.id.date_of_training)).getText().toString();
        String changedTitle = ((EditText) findViewById(R.id.title_edt)).getText().toString();
        Intent intent = new Intent();
        intent.putExtra(IntentConstants.INTENT_CHANGED_DATE, changedDate);
        intent.putExtra(IntentConstants.INTENT_CHANGED_TITLE, changedTitle);
        intent.putExtra(IntentConstants.INTENT_ITEM_POSITION, position);
        setResult(IntentConstants.INTENT_REQUEST_CODE_TWO, intent);
        finish();


    }
}
